#include <USIWire.h>

#define DRIVER_IN1_PIN 3 //PB3, chip pin 2
#define DRIVER_IN2_PIN 4 //PB4, chip pin 3
// what would be ENC_1_PIN is 5, PB5, ADC0, chip pin 1 (reset), set up in ADC below
#define ENC_2_PIN 1 //PB1, chip pin 6
#define INTERNAL_DIVIDE 20*4 // One count according to an external system
// is this many real counts
#define COUNT_ERROR_MARGIN 5 // Stop when close enough, must be < INTERNAL_DIVIDE
#define I2C_ADDR 0x40
#define ENABLE_CODE 0xAA
#define WRITE_START_CODE 0xCC
#define BUSY_CODE 0xBB
#define DONE_CODE 0xDD

volatile long enc_pos;
volatile int enc_pos_buf;
volatile unsigned char write_idx;
unsigned char enc_phase; // 0-3 make a full rotation
volatile bool writing;
unsigned int zero_count;

void setup() {
  pinMode(DRIVER_IN1_PIN, OUTPUT);
  pinMode(DRIVER_IN2_PIN, OUTPUT);
  Wire.begin(I2C_ADDR);
  Wire.onRequest(requestEvent);
  Wire.onReceive(receiveEvent);
  enc_pos = 0;
  enc_pos_buf = 0;
  writing = false;
  write_idx = 0;
  zero_count = 0;
  init_enc_phase();
}

void loop() {
  if (enc1read()) {
    if (enc2read()) {
      if (enc_phase == 2) {
        enc_pos++;
      } else if (enc_phase == 0) {
        enc_pos--;
      }
      enc_phase = 3;
    } else {
      if (enc_phase == 1) {
        enc_pos++;
      } else if (enc_phase == 3) {
        enc_pos--;
      }
      enc_phase = 2;
    }
  } else {
    if (enc2read()) {
      if (enc_phase == 3) {
        enc_pos++;
      } else if (enc_phase == 1) {
        enc_pos--;
      }
      enc_phase = 0;
    } else {
      if (enc_phase == 0) {
        enc_pos++;
      } else if (enc_phase == 2) {
        enc_pos--;
      }
      enc_phase = 1;
    }
  }
  if (enc_pos < -COUNT_ERROR_MARGIN) {
    // Drive forward
    digitalWrite(DRIVER_IN1_PIN, HIGH);
    digitalWrite(DRIVER_IN2_PIN, LOW);
  } else if (enc_pos > COUNT_ERROR_MARGIN) {
    // Drive backward
    digitalWrite(DRIVER_IN1_PIN, LOW);
    digitalWrite(DRIVER_IN2_PIN, HIGH);
  } else {
    // Stop near 0
    digitalWrite(DRIVER_IN1_PIN, LOW);
    digitalWrite(DRIVER_IN2_PIN, LOW);
  }
}

void init_enc_phase() {
  switch ((enc1read() << 1) + enc2read()) {
    case 3:
      enc_phase = 3;
      break;
    case 2:
      enc_phase = 2;
      break;
    case 1:
      enc_phase = 0;
      break;
    default:
      enc_phase = 1;
  }
}

bool enc1read() {
  return analogRead(0) > 900; // Reset pin
}

bool enc2read() {
  return digitalRead(ENC_2_PIN) != 0;
}

void requestEvent() {
  if (writing) {
    Wire.write((enc_pos_buf & 255) ^ ((enc_pos_buf >> 8) & 255)); // xor "checksum"
  } else {
    if (abs(enc_pos) <= COUNT_ERROR_MARGIN) {
      Wire.write(DONE_CODE);
    } else {
      Wire.write(BUSY_CODE);
    }
  }
}

void receiveEvent(int n) {
  // Master should send a lot of zeros, then the write start code, then write a
  // sequence of bytes to post-divide enc_pos, least significant byte first
  unsigned char in = Wire.read();
  if (in == WRITE_START_CODE && zero_count >= 2) {
    write_idx = 0;
    enc_pos_buf = 0;
    zero_count = 0;
    writing = true;
    return;
  }
  if (in == 0) {
    zero_count++;
  } else {
    zero_count = 0;
  }
  if (in == ENABLE_CODE) {// && write_idx == 2) {
    enc_pos = ((long)enc_pos_buf) * INTERNAL_DIVIDE;
    init_enc_phase();
    writing = false;
    return;
  }
  enc_pos_buf |= in << (8 * write_idx);
  while (Wire.available()) {
    Wire.read(); // clear read buffer, unnecessary if master behaves
  }
  write_idx++;
}


