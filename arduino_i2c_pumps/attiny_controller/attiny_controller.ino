#include <USIWire.h>

#define DRIVER_IN1_PIN 3 //PB3, chip pin 2
#define DRIVER_IN2_PIN 4 //PB4, chip pin 3
// what would be ENC_1_PIN is 5, PB5, ADC0, chip pin 1 (reset), set up in ADC below
#define ENC_2_PIN 1 //PB1, chip pin 6
#define COUNT_ERROR_MARGIN 5
#define I2C_ADDR 0x45

volatile int enc_pos;
volatile int write_idx;
char state;

void setup() {
  pinMode(DRIVER_IN1_PIN, OUTPUT);
  pinMode(DRIVER_IN2_PIN, OUTPUT);
  Wire.begin(I2C_ADDR);
  Wire.onRequest(requestEvent);
  Wire.onReceive(receiveEvent);
  enc_pos = 0;
  write_idx = sizeof(enc_pos); // start with valid enc_pos, ready to write
  init_state();
}

void loop() {
  if (write_idx < sizeof(enc_pos)) {
    // We're in the middle of a write to enc_pos, it's invalid
    digitalWrite(DRIVER_IN1_PIN, LOW);
    digitalWrite(DRIVER_IN2_PIN, LOW);
    init_state(); // keep state correct for whenever we're done writing
    return;
  }
  if (enc1read()) {
    if (enc2read()) {
      if (state == 2) {
        enc_pos++;
      } else if (state == 0) {
        enc_pos--;
      }
      state = 3;
    } else {
      if (state == 1) {
        enc_pos++;
      } else if (state == 3) {
        enc_pos--;
      }
      state = 2;
    }
  } else {
    if (enc2read()) {
      if (state == 3) {
        enc_pos++;
      } else if (state == 1) {
        enc_pos--;
      }
      state = 0;
    } else {
      if (state == 0) {
        enc_pos++;
      } else if (state == 2) {
        enc_pos--;
      }
      state = 1;
    }
  }
  if (enc_pos < -COUNT_ERROR_MARGIN) {
    // Drive forward
    digitalWrite(DRIVER_IN1_PIN, HIGH);
    digitalWrite(DRIVER_IN2_PIN, LOW);
  } else if (enc_pos > COUNT_ERROR_MARGIN) {
    // Drive backward
    digitalWrite(DRIVER_IN1_PIN, LOW);
    digitalWrite(DRIVER_IN2_PIN, HIGH);
  } else {
    // Stop near 0
    digitalWrite(DRIVER_IN1_PIN, LOW);
    digitalWrite(DRIVER_IN2_PIN, LOW);
  }
}

void init_state() {
  switch ((enc1read() << 1) + enc2read()) {
    case 3:
      state = 3;
      break;
    case 2:
      state = 2;
      break;
    case 1:
      state = 0;
      break;
    default:
      state = 1;
  }
}

bool enc1read() {
  return analogRead(0) > 900; // Reset pin
}

bool enc2read() {
  return digitalRead(ENC_2_PIN) != 0;
}

void requestEvent() {
  if (abs(enc_pos) <= COUNT_ERROR_MARGIN) {
    Wire.write(0x00);
  } else {
    Wire.write(0x01);
  }
}

void receiveEvent(int n) {
  // initialize write by sending 0
  // ensure write readiness state by sending an arbitrary number of 0s
  //   in a row > sizeof(enc_pos)
  // write sequence of bytes to enc_pos, least significant byte first
  unsigned char in = Wire.read();
  if (in == 0 && (write_idx >= sizeof(enc_pos) || enc_pos == 0)) {
    // received a 0 after enough bytes written to cover enc_pos or it was already 0
    // this is write reset
    write_idx = 0;
    enc_pos = 0;
    return;
  }
  enc_pos |= in << (8 * write_idx);
  while (Wire.available()) {
    Wire.read(); // clear read buffer, unecessary if used right
  }
  write_idx += 1;
}

